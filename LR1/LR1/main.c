#include <stdio.h>
#include <locale.h>
#include <stdint.h>
#include <math.h>

#define NUM_OF_CHARS		256
#define MAX_FILENAME_LEN	100

struct charCalculations {
	uint8_t character;
	int absolute_cnt;
	unsigned int ascii_code;
	double relative_freq;
	double log_2pi;
	double pi_multipliedBy_log2pi;
};

int main(void) {
	int total_chars = 0;
	int char_cnt[NUM_OF_CHARS] = { 0 };
	double output = 0;

	setlocale(LC_ALL, "Rus");
	char inputFilename[MAX_FILENAME_LEN];

	printf("�������� ������� ���� ������������. 09.03.02, 2023�.\n");
	printf("������� ��� ����� : \n");
	scanf("%s", inputFilename);

	FILE* inputFile = fopen(inputFilename, "r");
	if (!inputFile) { printf("�� �������� ������� ����..."); return 1; }

	// ������� ���������� ��������
	int curr_char = 0;
	while ((curr_char = fgetc(inputFile)) != EOF) {
		char_cnt[curr_char]++;
		total_chars++;
	}

	// ��������� ��������� � �����������
	struct charCalculations charTable[NUM_OF_CHARS];
	uint16_t char_idx = 0;
	for (uint16_t i = 0; i < NUM_OF_CHARS; i++) {
		if (char_cnt[i] > 0) {
			charTable[char_idx].character = (unsigned char)i;
			charTable[char_idx].absolute_cnt = char_cnt[i];
			charTable[char_idx].ascii_code = i;
			charTable[char_idx].relative_freq = (float)char_cnt[i] * 100 / total_chars;
			charTable[char_idx].log_2pi = log10(charTable[char_idx].relative_freq) /log10(2);
			charTable[char_idx].pi_multipliedBy_log2pi = charTable[char_idx].log_2pi * charTable[char_idx].relative_freq;
			output += charTable[char_idx].pi_multipliedBy_log2pi;
			char_idx++;
		}
	}

	// ����� ������� �� ����� � � �������� ����
	FILE* output_file = fopen("output.txt", "w");
	if (output_file == NULL) {
		printf("������ �������� ��������� �����\n");
		return 1;
	}

	printf("������\t���������� �������\t��� ASCII\t������������� �������\tLog 2Pi\tPi*Log_2Pi\n");
	fprintf(output_file, "������\t���������� �������\t��� ASCII\t������������� �������\tLog 2Pi\tPi*Log_2Pi\n");

	for (uint16_t i = 0; i < char_idx; i++) {
		printf("%c\t%d\t\t\t%d\t\t%.3f\t\t\t%.3f\t%.3f\n", charTable[i].character, charTable[i].absolute_cnt, charTable[i].ascii_code
			, charTable[i].relative_freq, charTable[i].log_2pi, charTable[i].pi_multipliedBy_log2pi);
	}

	printf("\n����� :%.3f", output);
	

	fclose(inputFile);
	fclose(output_file);

	return 0;
}
